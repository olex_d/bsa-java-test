package com.example.demo.exception;

public class ToDoNotFoundException extends Exception {

	public ToDoNotFoundException(Long id) {
		super(String.format("Can not find todo with id %d", id));
	}

}
