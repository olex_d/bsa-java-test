package com.example.demo.exception;

public class UserNotFoundException extends Exception {

	public UserNotFoundException(Long id) {
		super(String.format("Can not find user with id %d", id));
	}

}
