package com.example.demo;

import com.example.demo.model.ToDoEntity;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Profile;

@SpringBootApplication
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

	@Bean
	@Profile("develop")
	CommandLineRunner initDatabase(ToDoRepository todoRepository, UserRepository userRepository) {
		return args -> {
			var alex = userRepository
					.save(new UserEntity("Alexander", "olexander.danilchenko@gmail.com"));
			var max = userRepository
					.save(new UserEntity("Maxim", "maksim.goncharuk@binary-studio.com"));

			todoRepository.save(new ToDoEntity("Wash the dishes", alex));
			todoRepository.save(new ToDoEntity("Read lecture", alex));
			todoRepository.save(new ToDoEntity("Answer in chat", max));
		};
	}
}
