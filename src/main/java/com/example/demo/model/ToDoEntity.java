package com.example.demo.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

/**
 * ToDoEntity
 */
@Entity
@Table(name = "todos")
public class ToDoEntity {

	@Id
	@GeneratedValue
	@Column(nullable = false, updatable = false)
	private Long id;

	@NotBlank
	@Column
	private String text;

	@Column
	private ZonedDateTime completedAt;

	@ManyToOne(cascade = CascadeType.REFRESH, fetch = FetchType.LAZY)
	@JoinColumn(name = "user_id")
	private UserEntity user;

	public ToDoEntity() {
	}

	public ToDoEntity(String text, UserEntity user) {
		this.user = user;
		this.text = text;
	}

	public ToDoEntity(Long id, String text, UserEntity user) {
		this.id = id;
		this.text = text;
		this.user = user;
	}

	public ToDoEntity(Long id, String text, ZonedDateTime completedAt, UserEntity user) {
		this.id = id;
		this.text = text;
		this.completedAt = completedAt;
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public String getText() {
		return text;
	}

	public ToDoEntity setText(String text) {
		this.text = text;
		return this;
	}

	public ZonedDateTime getCompletedAt() {
		return completedAt;
	}

	public ToDoEntity completeNow() {
		completedAt = ZonedDateTime.now(ZoneOffset.UTC);
		return this;
	}

	public UserEntity getUser() {
		return user;
	}

	@Override
	public String toString() {
		return "ToDoEntity{" +
				"id=" + id +
				", text='" + text + '\'' +
				", completedAt=" + completedAt +
				'}';
	}
}
