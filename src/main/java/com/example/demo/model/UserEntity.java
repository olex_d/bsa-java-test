package com.example.demo.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "users")
public class UserEntity {

	@Id
	@GeneratedValue
	@Column(nullable = false, updatable = false)
	private Long id;

	@NotNull
	@Column(unique = true)
	private String username;

	@Column(unique = true)
	private String email;

	@OneToMany(mappedBy = "user", fetch = FetchType.LAZY, orphanRemoval = true)
	private final List<ToDoEntity> todos = new ArrayList<>();

	public UserEntity() {
	}

	public UserEntity(@NotNull String username, String email) {
		this.username = username;
		this.email = email;
	}

	public UserEntity(Long id, @NotNull String username, String email) {
		this.id = id;
		this.username = username;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public String getUsername() {
		return username;
	}

	public String getEmail() {
		return email;
	}

	public List<ToDoEntity> getTodos() {
		return todos;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "UserEntity{" +
				"id=" + id +
				", username='" + username + '\'' +
				", email='" + email + '\'' +
				", todos='" + todos.size() + '\'' +
				'}';
	}
}
