package com.example.demo.controller;

import com.example.demo.dto.user.UserResponse;
import com.example.demo.dto.user.UserSaveRequest;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.service.UserService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("users")
public class UserController {

	private final UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({UserNotFoundException.class})
	public String handleException(Exception ex) {
		return ex.getMessage();
	}

	@GetMapping
	@Valid List<UserResponse> getAll() {
		return userService.getAll();
	}

	@GetMapping("/{id}")
	@Valid UserResponse getUser(@PathVariable Long id) throws UserNotFoundException {
		return userService.getUserResponse(id);
	}

	@PostMapping
	@Valid UserResponse saveUser(@RequestBody UserSaveRequest saveRequest) throws UserNotFoundException {
		return userService.upsert(saveRequest);
	}

}
