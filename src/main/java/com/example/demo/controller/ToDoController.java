package com.example.demo.controller;

import com.example.demo.dto.todo.ToDoResponse;
import com.example.demo.dto.todo.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.service.ToDoService;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/todos")
public class ToDoController {

	private final ToDoService toDoService;

	public ToDoController(ToDoService toDoService) {
		this.toDoService = toDoService;
	}

	@ResponseStatus(HttpStatus.NOT_FOUND)
	@ExceptionHandler({ToDoNotFoundException.class})
	public String handleException(Exception ex) {
		return ex.getMessage();
	}

	@GetMapping
	@Valid List<ToDoResponse> getAll() {
		return toDoService.getAll();
	}

	@GetMapping("/{id}")
	@Valid ToDoResponse getOne(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.getOne(id);
	}

	@PostMapping
	@Valid ToDoResponse save(@Valid @RequestBody ToDoSaveRequest todoSaveRequest)
			throws ToDoNotFoundException, UserNotFoundException {
		return toDoService.upsert(todoSaveRequest);
	}

	@PutMapping("/{id}/complete")
	@Valid ToDoResponse save(@PathVariable Long id) throws ToDoNotFoundException {
		return toDoService.completeToDo(id);
	}

	@DeleteMapping("/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	void delete(@PathVariable Long id) {
		toDoService.deleteOne(id);
	}

	@DeleteMapping("/user/{userId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	void deleteUserTodos(@PathVariable Long userId) throws UserNotFoundException {
		toDoService.deleteUserTodos(userId);
	}

}
