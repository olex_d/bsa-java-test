package com.example.demo.dto.user;

import com.example.demo.dto.todo.ToDoResponse;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.List;

public class UserResponse {

	@NotNull
	public Long id;

	@NotBlank
	public String username;

	public String email;

	public List<ToDoResponse> todos;

}
