package com.example.demo.dto.user;

import javax.validation.constraints.NotBlank;

public class UserSaveRequest {

	public Long id;

	@NotBlank
	public String username;

	public String email;

	public UserSaveRequest(@NotBlank String username, String email) {
		this.username = username;
		this.email = email;
	}

	public UserSaveRequest(Long id, @NotBlank String username, String email) {
		this.id = id;
		this.username = username;
		this.email = email;
	}

}
