package com.example.demo.dto.user.mapper;

import com.example.demo.dto.todo.mapper.ToDoEntityToResponseMapper;
import com.example.demo.dto.user.UserResponse;
import com.example.demo.model.UserEntity;

import java.util.Objects;
import java.util.stream.Collectors;

public class UserResponseMapper {
	public static UserResponse map(UserEntity userEntity) {
		if (Objects.isNull(userEntity)) return null;

		var result = new UserResponse();
		result.id = userEntity.getId();
		result.username = userEntity.getUsername();
		result.email = userEntity.getEmail();
		result.todos = userEntity.getTodos()
				.stream()
				.map(ToDoEntityToResponseMapper::map)
				.collect(Collectors.toList());
		return result;
	}
}
