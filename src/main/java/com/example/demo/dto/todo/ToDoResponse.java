package com.example.demo.dto.todo;

import javax.validation.constraints.NotNull;
import java.time.ZonedDateTime;

public class ToDoResponse {

	@NotNull
	public Long id;

	@NotNull
	public String text;

	public ZonedDateTime completedAt;

	@NotNull
	public Long userId;

}
