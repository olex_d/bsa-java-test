package com.example.demo.dto.todo;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

public class ToDoSaveRequest {

	public Long id;

	@NotBlank
	public String text;

	@NotNull
	public Long userId;

	public ToDoSaveRequest(@NotNull String text, @NotNull Long userId) {
		this.text = text;
		this.userId = userId;
	}

	public ToDoSaveRequest(Long id, @NotNull String text, @NotNull Long userId) {
		this.id = id;
		this.text = text;
		this.userId = userId;
	}

}
