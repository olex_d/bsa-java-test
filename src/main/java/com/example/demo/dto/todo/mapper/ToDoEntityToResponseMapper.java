package com.example.demo.dto.todo.mapper;

import com.example.demo.dto.todo.ToDoResponse;
import com.example.demo.model.ToDoEntity;

import java.util.Objects;

public class ToDoEntityToResponseMapper {
	public static ToDoResponse map(ToDoEntity todoEntity) {
		if (Objects.isNull(todoEntity)) return null;

		var result = new ToDoResponse();
		result.id = todoEntity.getId();
		result.text = todoEntity.getText();
		result.completedAt = todoEntity.getCompletedAt();
		result.userId = todoEntity.getUser().getId();
		return result;
	}
}
