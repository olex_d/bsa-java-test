package com.example.demo.service;

import com.example.demo.dto.user.UserResponse;
import com.example.demo.dto.user.UserSaveRequest;
import com.example.demo.dto.user.mapper.UserResponseMapper;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class UserService {

	private final UserRepository userRepository;

	public UserService(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	public List<UserResponse> getAll() {
		return userRepository.findAll()
				.stream()
				.map(UserResponseMapper::map)
				.collect(Collectors.toList());
	}

	public UserEntity getUser(Long id) throws UserNotFoundException {
		return userRepository.findById(id)
				.orElseThrow(() -> new UserNotFoundException(id));
	}

	public UserResponse getUserResponse(Long id) throws UserNotFoundException {
		return UserResponseMapper.map(getUser(id));
	}

	public UserResponse upsert(UserSaveRequest saveRequest) throws UserNotFoundException {
		UserEntity userEntity;
		if (Objects.isNull(saveRequest.id)) {
			userEntity = new UserEntity(
					saveRequest.username,
					saveRequest.email
			);
		} else {
			userEntity = getUser(saveRequest.id);
			userEntity.setUsername(saveRequest.username);
			userEntity.setEmail(saveRequest.email);
		}
		return UserResponseMapper.map(userRepository.save(userEntity));
	}

}
