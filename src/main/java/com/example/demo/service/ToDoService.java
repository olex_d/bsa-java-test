package com.example.demo.service;

import com.example.demo.dto.todo.ToDoResponse;
import com.example.demo.dto.todo.ToDoSaveRequest;
import com.example.demo.dto.todo.mapper.ToDoEntityToResponseMapper;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.repository.ToDoRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Service
public class ToDoService {

	private final ToDoRepository toDoRepository;
	private final UserService userService;

	public ToDoService(ToDoRepository toDoRepository, UserService userService) {
		this.toDoRepository = toDoRepository;
		this.userService = userService;
	}

	public List<ToDoResponse> getAll() {
		return toDoRepository
				.findAll()
				.stream()
				.map(ToDoEntityToResponseMapper::map)
				.collect(Collectors.toList());
	}

	public ToDoEntity getTodo(Long id) throws ToDoNotFoundException {
		return toDoRepository
				.findById(id)
				.orElseThrow(() -> new ToDoNotFoundException(id));
	}

	public ToDoResponse upsert(ToDoSaveRequest toDoDTO) throws ToDoNotFoundException, UserNotFoundException {
		var user = userService.getUser(toDoDTO.userId);

		ToDoEntity todo;
		if (Objects.isNull(toDoDTO.id)) {
			todo = new ToDoEntity(toDoDTO.text, user);
		} else {
			todo = getTodo(toDoDTO.id);
			todo = todo.setText(toDoDTO.text);
		}
		return ToDoEntityToResponseMapper.map(toDoRepository.save(todo));
	}

	public ToDoResponse completeToDo(Long id) throws ToDoNotFoundException {
		ToDoEntity todo = getTodo(id);
		todo = todo.completeNow();
		return ToDoEntityToResponseMapper.map(toDoRepository.save(todo));
	}

	public ToDoResponse getOne(Long id) throws ToDoNotFoundException {
		return ToDoEntityToResponseMapper.map(getTodo(id));
	}

	public void deleteOne(Long id) {
		toDoRepository.deleteById(id);
	}

	@Transactional // allow to upload lazy todos list from user
	public void deleteUserTodos(Long userId) throws UserNotFoundException {
		var user = userService.getUser(userId);
		user.getTodos().forEach(todo -> this.deleteOne(todo.getId()));
	}
}
