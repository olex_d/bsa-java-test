package com.example.demo.service;

import com.example.demo.dto.user.mapper.UserResponseMapper;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.samePropertyValuesAs;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class UserServiceTest {

	private UserRepository userRepository;
	private UserService userService;

	@BeforeEach
	void setUp() {
		userRepository = mock(UserRepository.class);
		userService = new UserService(userRepository);
	}

	@Test
	void whenGetUserResponseWithId_thenReturnResponse() throws UserNotFoundException {
		// mock
		var expectedUser = new UserEntity(0L, "Alex", "olexa@gmail.com");
		when(userRepository.findById(anyLong())).thenAnswer(i -> Optional.of(expectedUser));

		// call
		var foundUser = userService.getUserResponse(expectedUser.getId());

		// verify
		assertThat(UserResponseMapper.map(expectedUser), samePropertyValuesAs(foundUser));
	}

	@Test
	void whenGetUserResponseNoId_thenThrowUserNotFoundException() {
		// mock
		var expectedUser = new UserEntity(0L, "Alex", "olexa@gmail.com");
		when(userRepository.findById(anyLong())).thenAnswer(i -> Optional.empty());

		// call/validate
		assertThrows(UserNotFoundException.class, () ->
				userService.getUserResponse(expectedUser.getId())
		);
	}

}
