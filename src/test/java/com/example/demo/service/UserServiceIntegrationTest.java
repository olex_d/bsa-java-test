package com.example.demo.service;

import com.example.demo.controller.UserController;
import com.example.demo.model.ToDoEntity;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Import;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@WebMvcTest(UserController.class)
@ActiveProfiles(profiles = "test")
@Import(UserService.class)
public class UserServiceIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@MockBean
	private UserRepository userRepository;

	@Test
	void whenGetAllUsers_thenReturnValidResponse() throws Exception {
		// mock
		var max = new UserEntity("Maxim", "maksim.goncharuk@binary-studio.com");
		var kiril = new UserEntity("Kiril", "kirill.kandel@gmail.com");
		var alex = new UserEntity(
				"Alexander",
				"olexander.danylchenko@gmail.com");
		var userTodos = List.of(
				new ToDoEntity(1L, "Wash plate", alex),
				new ToDoEntity(2L, "Watch video", alex)
		);
		alex.getTodos().addAll(userTodos);

		var users = List.of(max, kiril, alex);

		when(userRepository.findAll()).thenReturn(users);

		// call/validate
		mockMvc
				.perform(get("/users"))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isArray())
				.andExpect(jsonPath("$", hasSize(users.size())))
				.andExpect(jsonPath("$[2].username").value("Alexander"))
				.andExpect(jsonPath("$[2].todos").isArray())
				.andExpect(jsonPath("$[2].todos", hasSize(userTodos.size())));
	}

}
