package com.example.demo.service;

import com.example.demo.dto.todo.ToDoSaveRequest;
import com.example.demo.dto.user.UserSaveRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
public class TodoServiceIntegrationTest {

	@Autowired
	private MockMvc mockMvc;

	@Autowired
	private UserService userService;

	@Autowired
	private ToDoService todoService;

	@Test
	void whenAddProperTodo_thenSaveToUser() throws Exception {
		// mock
		var user = new UserSaveRequest("Alexander", "olexander.danilchenko@gmail.com");
		var userResponse = userService.upsert(user);

		var todo = new ToDoSaveRequest("Some action", userResponse.id);
		todoService.upsert(todo);

		// call/validate
		mockMvc
				.perform(get("/users/" + userResponse.id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isMap())
				.andExpect(jsonPath("$.username").value(userResponse.username))
				.andExpect(jsonPath("$.todos").isArray())
				.andExpect(jsonPath("$.todos", hasSize(1)));
	}

	@Test
	void whenDeleteProperTodo_thenDeleteFromUser() throws Exception {
		// mock
		var expectedUser = new UserSaveRequest("Alexander", "olexander.danilchenko@gmail.com");
		var userResponse = userService.upsert(expectedUser);

		var todo1 = new ToDoSaveRequest("Watch movie", userResponse.id);
		var todo1Response = todoService.upsert(todo1);
		var todo2 = new ToDoSaveRequest("Lie on the couch", userResponse.id);
		todoService.upsert(todo2);

		// call
		todoService.deleteOne(todo1Response.id);

		// validate
		mockMvc
				.perform(get("/users/" + userResponse.id))
				.andExpect(status().isOk())
				.andExpect(content().contentType(MediaType.APPLICATION_JSON))
				.andExpect(jsonPath("$").isMap())
				.andExpect(jsonPath("$.username").value(userResponse.username))
				.andExpect(jsonPath("$.todos").isArray())
				.andExpect(jsonPath("$.todos", hasSize(1)));
	}

	@Test
	void whenGetDeletedUserTodo_thenThrowTodoNotFoundException() throws Exception {
		// mock
		var expectedUser = new UserSaveRequest("Alexander", "olexander.danilchenko@gmail.com");
		var userResponse = userService.upsert(expectedUser);

		var todo1 = new ToDoSaveRequest("Watch movie", userResponse.id);
		var todo1response = todoService.upsert(todo1);
		var todo2 = new ToDoSaveRequest("Lie on the couch", userResponse.id);
		var todo2response = todoService.upsert(todo2);

		// call
		todoService.deleteUserTodos(userResponse.id);

		// validate TodoNotFoundException thrown
		mockMvc
				.perform(get("/todos/" + todo1response.id))
				.andExpect(status().isNotFound());
		mockMvc
				.perform(get("/todos/" + todo2response.id))
				.andExpect(status().isNotFound());
	}

}
