package com.example.demo.service;

import com.example.demo.dto.todo.ToDoSaveRequest;
import com.example.demo.exception.ToDoNotFoundException;
import com.example.demo.exception.UserNotFoundException;
import com.example.demo.model.ToDoEntity;
import com.example.demo.model.UserEntity;
import com.example.demo.repository.ToDoRepository;
import com.example.demo.repository.UserRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentMatchers;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertSame;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TodoServiceTest {

	private UserRepository userRepository;

	private ToDoRepository toDoRepository;
	private ToDoService toDoService;

	@BeforeEach
	void setUp() {
		userRepository = mock(UserRepository.class);
		var userService = new UserService(userRepository);

		toDoRepository = mock(ToDoRepository.class);
		toDoService = new ToDoService(toDoRepository, userService);
	}

	@Test
	void whenUpsertWithUserId_thenReturnNew() throws ToDoNotFoundException, UserNotFoundException {
		// mock
		var expectedUser = new UserEntity(0L, "Alexander", "olexander.danylchenko@gmail.com");
		when(userRepository.findById(anyLong())).thenAnswer(i -> Optional.of(expectedUser));

		var newTodo = new ToDoEntity("Write unit test", expectedUser);
		when(toDoRepository.save(ArgumentMatchers.any(ToDoEntity.class))).thenAnswer(i -> newTodo);
		when(toDoRepository.findById(anyLong())).thenAnswer(i -> Optional.of(newTodo));

		// call
		var todoSave = new ToDoSaveRequest(newTodo.getId(), newTodo.getText(), newTodo.getUser().getId());
		var saved = toDoService.upsert(todoSave);

		// validate
		assertSame(todoSave.id, saved.id);
		assertSame(todoSave.text, saved.text);
		assertSame(todoSave.userId, saved.userId);
	}

	@Test
	void whenUpsertWithNoUserId_thenThrowUserNotFoundException() {
		// mock
		var newTodo = new ToDoSaveRequest("Play HL2", null);
		when(userRepository.findById(anyLong())).thenAnswer(i -> Optional.empty());

		// call/validate
		assertThrows(UserNotFoundException.class, () ->
				toDoService.upsert(newTodo)
		);
	}

}
